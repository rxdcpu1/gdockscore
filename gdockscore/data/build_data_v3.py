"""
Go through interacting chains in PDB biounits and perform global and local docking to
generate a data set for model training

Here a positive example (binder) has a CAPRI rank >= 1 and negative example has a CAPRI
rank of 0

This dataset is designed to be used for pretraining a scoring model on general "good"
and "bad" interfaces for use with transfer learning.
"""

import argparse
from Bio import PDB
import glob
import logging
import os
import pandas as pd
import shutil
import subprocess
from multiprocessing import Pool
import multiprocessing
import numpy as np
import re

# import numpy as np
# import random
logging.basicConfig(
    filename="data_builder.log", level=logging.ERROR
)  # Basic error logging


class DataBuilder:
    """A class used to compile a database of positive examples from the PDB biounit
    files.

    The class finds interacting chains within biological assemblies (biounits) and saves
    all discovered pairs in a database.

    Attributes
    ----------
    root_dir : str
        The location of the PDB files on disk
    parser : class
        The PDB parser class used to conduct operations on the files
    og_dir : str
        The original file path from which the script was started
    save_dir : str
        The location to save the docking outputs
    db : class
        Pandas dataframe containing the input PDBs and chain pairs
    num_local : int
        The number of local docking decoys to generate
    num_global : int
        The number of global docking decoys to generate

    Methods
    -------
    build()
        Builds the database and stores in a list of tuples in the
        format (file, chain_1, chain_2)
    process_output()
        Processes the output of the build function giving each file a unique file name
        and label where a positive example has CAPRI rank >= 1 and a negative example
        has a CAPRI rank = 0
    """

    def __init__(
        self, root_dir, save_dir, db_path, num_local=3, num_global=3, thresh=1
    ):
        self.root_dir = root_dir
        self.parser = PDB.PDBParser(QUIET=True)
        self.save_dir = save_dir
        self.og_dir = os.getcwd()
        self.db = pd.read_csv(db_path, header=0)
        # Number of local and global docks to produce
        self._num_local = num_local
        self._num_global = num_global
        self._thresh = thresh

    @property
    def num_local(self):
        return self._num_local

    @num_local.setter
    def num_local(self, new_num):
        self._num_local = new_num

    @property
    def num_global(self):
        self._num_local

    @num_global.setter
    def num_global(self, new_num):
        self._num_global = new_num

    @property
    def thresh(self):
        return self._thresh

    @thresh.setter
    def thresh(self, new_thresh):
        self._thresh = new_thresh

    def _find_biounit_path(self, biounit):
        """Find pdb path in root directory"""
        for curr_root, dirs, files in os.walk(self.root_dir):
            if biounit in files:
                path = os.path.join(curr_root, biounit)
                return path

        return None

    # def _extract_chains(self, biounit_path, chains, file_name):
    #     """Extract relevant chains from a PDB"""
    #     command = (
    #         "pdb_selchain -"
    #         + ",".join(chains)
    #         + " "
    #         + biounit_path
    #         + " | pdb_delhetatm | pdb_tidy > "
    #         + file_name
    #     )  # Delete heteroatoms, and clean up file as well
    #     subprocess.Popen(command, shell=True).wait()
    #
    #     return None

    def _extract_chains(self, biounit_path, chains):
        """Extract relevant chains from a PDB"""
        command = (
            "python2 ~/rosetta/tools/protein_tools/scripts/clean_pdb.py "
            + biounit_path
            + " "
            + "".join(chains)
        )  # Have to use Rosetta clean_pdb tool as otherwise the docking protocol
        # randomly inserts TER before docking creating malformed backbones
        subprocess.Popen(command, shell=True).wait()

        return None

    def _prepack(self, biounit_path):
        """Prepack the structure with RosettaDock"""
        command = (
            "~/rosetta/source/bin/docking_prepack_protocol.static.linuxgccrelease -s "
            + biounit_path
            + " -overwrite"
        )
        subprocess.Popen(command, shell=True).wait()

        return None

    def _local_dock(self, biounit_path):
        """Perform a local dock of two chains in their native structure

        TODO: Make the perturbation values input variables

        NOTE: -dock_pert 0.5 0.5 originally used increased to 1.5 1.5
        """
        command = (
            "~/rosetta/source/bin/docking_protocol.static.linuxgccrelease -ex1 -ex2aro -dock_pert 1.5 1.5 -nstruct "
            + str(self._num_local)
            + " -s "
            + biounit_path
            + " -out:file:silent local.sb"
            + " -out:file:scorefile local.sc"
            + " -overwrite"
        )
        subprocess.Popen(command, shell=True).wait()

        return None

    def _global_dock(self, biounit_path):
        """Perform global docking"""
        command = (
            "~/rosetta/source/bin/docking_protocol.static.linuxgccrelease -randomize1 -randomize2"
            + " -ex1 -ex2aro -dock_pert 3 8 -nstruct "
            + str(self._num_global)
            + " -s "
            + biounit_path
            + " -out:file:silent global.sb"
            + " -out:file:scorefile global.sc"
            + " -overwrite"
        )  # Locations of two chains are randomized
        subprocess.Popen(command, shell=True).wait()

        return None

    def _full_dock(self, biounit, chain_1, chain_2):
        """Full docking process"""
        # Make folders
        try:
            new_path = os.path.join(
                self.save_dir, "working", biounit + "_" + chain_1 + chain_2
            )
            final_path = os.path.join(new_path, biounit)
            os.mkdir(new_path)
            os.chdir(new_path)
        except OSError:
            logging.error("Working directory already exists")
            final_path = os.path.join(new_path, biounit)
            os.chdir(new_path)

        try:
            # biounit, chain_1, chain_2 = row[:3]
            biounit_path = self._find_biounit_path(biounit)

            # Move file to working directory
            # new_path = os.path.join(working_path, biounit)
            shutil.copy(biounit_path, final_path)

            # Extract the relevant chains and dock
            # current_name = biounit.replace(".pdb", "") + "_extracted.pdb"
            self._extract_chains(final_path, chain_1 + chain_2)
            # current_name = (
            #     biounit.replace(".pdb", "") + "_" + chain_1 + chain_2 + ".pdb"
            # )
            if biounit.endswith("1"):
                current_name = biounit[:4] + "_" + chain_1 + chain_2 + ".pdb"
            else:
                current_name = biounit + "_" + chain_1 + chain_2 + ".pdb"
            self._prepack(current_name)
            os.remove("score.sc")
            current_name = current_name[:-4] + "_0001.pdb"
            # This fixes an issue where files named things like 1acb.pdb3_0001.pdb would
            # break the extract_pdbs tool
            new_name = current_name.replace(".pdb", "") + ".pdb"  # Simply remove all
            # instances of ".pdb"
            shutil.move(current_name, new_name)
            self._local_dock(new_name)
            self._global_dock(new_name)

            # Set the paths
            current_save_dir = os.path.join(self.save_dir, biounit)
            current_local = os.path.join(current_save_dir, chain_1 + chain_2, "local")
            current_global = os.path.join(current_save_dir, chain_1 + chain_2, "global")

            try:
                os.mkdir(current_save_dir)
            except OSError:
                logging.error("Target directory already exists")

            # Make the output directory and move the files
            try:
                os.mkdir(os.path.join(current_save_dir, chain_1 + chain_2))
                os.mkdir(current_local)
                os.mkdir(current_global)
            except OSError:
                logging.error("Target directory already exists")

            shutil.copy("local.sb", current_local)
            shutil.copy("global.sb", current_global)
            shutil.copy("local.sc", current_local)
            shutil.copy("global.sc", current_global)

            # Extract and save iRMSD
            # with open("score.sc", "r") as score_file:
            #     lines = score_file.readlines()
            #     last_list = lines[-1]
            #     irmsd = None

            # Clean up
            files_to_delete = glob.glob("*")

            for file in files_to_delete:
                os.remove(file)

        except Exception as e:
            logging.error(
                "Data point "
                + str(biounit)
                + " "
                + chain_1
                + " "
                + chain_2
                + " failed with error "
                + str(e)
            )

            # files_to_delete = glob.glob("*")

            # for file in files_to_delete:
            #     os.remove(file)

        return None

    def build(self):
        """Dock biounit chains to generate a data set"""
        working_path = os.path.join(self.save_dir, "working")

        try:
            os.mkdir(working_path)
        except OSError:
            print("Working directory exists")

        # Process all files in
        values = self.db.values.tolist()

        with Pool(multiprocessing.cpu_count()) as p:
            results = p.starmap(self._full_dock, values)

        return results

    def _extract_pdbs(self, silent_file):
        """Extract PDBs from a silent file"""
        command = (
            "~/rosetta/source/bin/extract_pdbs.static.linuxgccrelease -in:file:silent "
            + silent_file
            + " -overwrite"
        )
        subprocess.Popen(command, shell=True).wait()

        return None

    def process_output(self):
        """Rename files to biounit + unique ID and store label in file"""
        output = []
        # letters = "abcdefghijklmnopqrz"

        for curr_root, dirs, files in os.walk(self.save_dir):
            # Store potential paths
            global_score = os.path.join(curr_root, "global.sc")
            global_sb = os.path.join(curr_root, "global.sb")
            local_score = os.path.join(curr_root, "local.sc")
            local_sb = os.path.join(curr_root, "local.sb")
            try:
                if os.path.isfile(global_score):  # Check if current folder has files
                    score_df = pd.read_csv(global_score, delimiter=r"\s+", header=1)[
                        ["description", "CAPRI_rank"]
                    ]
                    print("Extracting global decoys...")
                    os.chdir(curr_root)
                    self._extract_pdbs(global_sb)
                    name_append = "global"
                elif os.path.isfile(local_score):
                    print("Extracting local decoys...")
                    score_df = pd.read_csv(local_score, delimiter=r"\s+", header=1)[
                        ["description", "CAPRI_rank"]
                    ]
                    os.chdir(curr_root)
                    self._extract_pdbs(local_sb)
                    name_append = "local"
                else:
                    continue  # Skip folders that do not contain files

            except Exception as e:
                logging.error("Skipping folder due to error " + str(e))
                continue

            # Iterate through the score file to get CAPRI rank and assign label
            for index, row in score_df.iterrows():
                # Generate a unique new name
                name = row[0] + ".pdb"
                chains = (
                    re.search(r"\_..\_", name)[0][1] + re.search(r"\_..\_", name)[0][2]
                )
                rand_int = np.random.randint(1, 1000000)
                new_name = name_append + "_" + str(rand_int) + "_" + name  # Specify if
                # file is global or local and add an id number
                # new_name = name_append + "_" + name
                # new_name = (
                #     old_name.replace("_extracted", "")
                #     + "_"
                #     + random.choice(letters)
                #     + random.choice(letters)
                #     + "_"
                #     + str(np.random.randint(1, 1000000))
                #     + ".pdb"
                # )
                try:  # Handle any files that fail to extract or have bugged scores
                    shutil.move(name, new_name)  # Rename the file to a unique ID

                    # Store filename and label
                    if row[1] >= self._thresh:
                        output.append([new_name, chains[0], chains[1], row[1], 1])
                    else:
                        output.append([new_name, chains[0], chains[1], row[1], 0])
                except Exception as e:
                    logging.error(
                        "The pdb " + new_name + " failed with error: " + str(e)
                    )
                    continue

        return output


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--root_dir",
        dest="root_dir",
        type=str,
        required=True,
        help="The path to the pdb biounit files.",
    )
    parser.add_argument(
        "--save_dir",
        dest="save_dir",
        type=str,
        required=True,
        help="The directory to save the files.",
    )
    parser.add_argument(
        "--db_path",
        dest="db_path",
        type=str,
        required=True,
        help="The path to the DB of chain pairs.",
    )

    args = parser.parse_args()
    root_dir = args.root_dir
    save_dir = args.save_dir
    db_path = args.db_path

    # Run code on inputs
    databuilder = DataBuilder(root_dir, save_dir, db_path)
    results = databuilder.build()  # Comment out of generation already complete
    labels = databuilder.process_output()

    # Generate final csv files and save
    os.chdir(databuilder.og_dir)
    # This DF is no longer necessary
    # df_success = pd.DataFrame.from_records(
    #     successful_files, columns=["File", "Chain1", "Chain2"]
    # ).to_csv("success.csv", index=False)
    df_unique = pd.DataFrame.from_records(
        labels, columns=["File", "Chain1", "Chain2", "CAPRI_rank", "Label"]
    ).to_csv("unique.csv", index=False)
