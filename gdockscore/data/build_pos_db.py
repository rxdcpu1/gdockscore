"""
Go through the PDB files of biounits and select
protein chain pairs that have a certain number of residues within a threshold
distance from each other

TODO: Create a new object attribute for the minimum number of interacting
residues to define the protein chains to be interacting. Find a way to
effectively chunk the numpy arrays so that smaller numbers of pairwise
distances are calculated
"""

from Bio import PDB
import numpy as np
import pandas as pd
from scipy.spatial import cKDTree
import os
import itertools
import argparse
import sys


class PositiveDatabaseBuilder:
    """A class used to compile a database of positive examples from the PDB biounit
    files.

    The class finds interacting chains within biological assemblies (biounits) and saves
    all discovered pairs in a database.

    Attributes
    ----------
    root_dir : str
        The location of the PDB files on disk
    parser : class
        The PDB parser class used to conduct operations on the files
    thresh : int
        The distance in angstroms two heavy atoms need to be within to
        be considered
    num_int : int
        The minimum number of residues within the specified threshold to
        consider the two chains to be interacting

    Methods
    -------
    fetch_chain(pdb_file, chain)
        Grabs a chain from a PDB file that has been parsed
    chain_distance(chain_a, chain_b, thresh)
        Checks how many atoms are within the distance threshold
        and returns true if there are enough atoms meeting the
        criteria
    get_atoms(chain_a, chain_b)
        Fetch the coordinates of the alpha carbons making up the chain backbones
        and store them in numpy arrays
    build_db_from_dir()
        Builds the database and stores in a list of tuples in the
        format (file, chain_1, chain_2)
    save_db()
        Saves the results into a CSV file using Pandas
    """

    def __init__(self, root_dir, thresh=6, num_int=3):

        self.root_dir = root_dir
        self.parser = PDB.PDBParser(QUIET=True)
        self.thresh = thresh
        # Have to keep in mind that some distance metrics calculate nxn matrix
        # doubling each distance because dij = dji
        self.num_int = num_int

        # Initialize a list that will contain the tuples of PDB file, chain 1, chain 2
        # self.result = []

    def fetch_chain(self, pdb_file, chain):
        # parser = PDB.PDBparser()
        prot_struct = self.parser.get_structure("input_prot", pdb_file)

        # Select chain
        prot_struct = prot_struct[0]
        chain = prot_struct[chain]

        return chain

    def get_atoms(self, chain_a, chain_b):
        # Fetch atoms
        atoms_a = []
        atoms_b = []

        for res_a in chain_a:
            try:
                atom_a = res_a["CA"].get_coord()
                atoms_a.append(atom_a)

            except KeyError:
                continue

        for res_b in chain_b:
            try:
                atom_b = res_b["CA"].get_coord()
                atoms_b.append(atom_b)

            except KeyError:
                continue

        atoms_a = np.array(atoms_a)
        atoms_b = np.array(atoms_b)

        return atoms_a, atoms_b

    def chain_distance(self, chain_a, chain_b, thresh):
        count = 0

        num_res_a = len([_ for _ in chain_a.get_residues() if PDB.is_aa(_)])
        num_res_b = len([_ for _ in chain_b.get_residues() if PDB.is_aa(_)])

        # Attempt to fix memory issues when dealing with large proteins by doing
        # one distance at a time (slow but more stable)
        if num_res_a > 1000 or num_res_b > 1000:
            for res_a in chain_a:
                for res_b in chain_b:
                    try:
                        dist = np.linalg.norm(
                            res_a["CA"].get_coord() - res_b["CA"].get_coord()
                        )

                        if dist <= self.thresh:
                            count = count + 1

                        # Test if number of residues in contact necessary to
                        # define interaction has been reached
                        if count >= self.num_int:
                            return True

                    except KeyError:
                        continue

        else:
            # Fetch the atoms
            atoms_a, atoms_b = self.get_atoms(chain_a, chain_b)

            # Construct the cKDTrees and find sparse distance matrix
            tree_a = cKDTree(atoms_a)
            tree_b = cKDTree(atoms_b)

            # Finds distances between points in cKDTrees and sets to 0 and
            # distances greater than the assigned threshold
            sparse_distance = tree_a.sparse_distance_matrix(tree_b, self.thresh, p=2.0)
            sparse_distance = sparse_distance.toarray()

            # If there are at least 3 distances that are within the threshold we
            # can say the chains are interacting
            if np.count_nonzero(sparse_distance) >= self.num_int:
                return True

        return False

    def build_db_from_dir(self, file_name):
        # Fetch the files in the root directory
        files = [file for file in os.listdir(self.root_dir)]

        count = 0

        with open(file_name, "a") as db_file:
            header = "File" + "\t" + "Chain 1" + "\t" + "Chain 2"
            db_file.write(header + "\n")

            for file in files:
                count = count + 1
                print(file)

                if count % 100 == 0:
                    # Print current file to sdout for Slurm logging
                    print(file)
                    sys.stdout.flush()

                    # Periodically write to file
                    db_file.flush()
                    os.fsync(db_file)

                file_path = self.root_dir + "/" + file

                try:
                    prot_struct = self.parser.get_structure("prot", file_path)

                except ValueError:
                    # If the PDB file fails to read continue to next file
                    continue

                chains = prot_struct[0]

                chain_list = []

                for chain in chains:
                    # chain_list.append(chain.to_upper())
                    # print(chain.get_id())
                    chain_id = chain.get_id()
                    chain_list.append(chain_id)

                if len(chain_list) > 1:
                    # Generate all possible pairings of chains
                    combinations = itertools.combinations(chain_list, 2)

                    for combination in combinations:
                        try:
                            chain_a = self.fetch_chain(file_path, combination[0])
                            chain_b = self.fetch_chain(file_path, combination[1])

                            if (
                                self.chain_distance(chain_a, chain_b, self.thresh)
                                is True
                            ):
                                # Append successful chain pairings to the database
                                # file
                                entry = (
                                    file + "\t" + combination[0] + "\t" + combination[1]
                                )
                                db_file.write(entry + "\n")

                            else:
                                continue
                        except Exception:
                            continue
                else:
                    continue

        return None

    def convert_to_csv(self, file_name):
        # Convert the list of tuples to a Pandas dataframe, name columns appropriately
        # and save to a csv file for reference on disk
        df = pd.read_csv(file_name, sep="\t")
        df.to_csv(file_name, sep="\t")

        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Set parser arguments
    parser.add_argument(
        "-rootdir",
        dest="root_dir",
        required=True,
        type=str,
        help="The root directory of PDB files to extract chain pairs from",
    )
    parser.add_argument(
        "-filename",
        dest="file_name",
        required=True,
        type=str,
        help="The file name for the chain pairs",
    )

    args = parser.parse_args()

    # Set root directory
    root_dir = args.root_dir
    file_name = args.file_name

    # Initialize class
    pos_db = PositiveDatabaseBuilder(root_dir, thresh=6, num_int=3)

    # Build and save database of pairs
    pos_db.build_db_from_dir(file_name)
