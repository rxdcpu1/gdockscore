"""
Authors: Osama Abdin, Matthew McFee

TODO: Orientations are found for all atoms in the opposite protein in the get
interface orientation features. This is inefficient but required little to no
change of code to get things running. Update the code for get_interface to
accept pi.build() output as input to avoid recalculating each time.

Removed chi angles since side chain directions should be sufficient
"""

from Bio import PDB
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
from scipy.spatial.transform import Rotation
import numpy as np
from gdockscore.data.side_chain_structure import fetch_atom_distances
from gdockscore.data.interface_structure import ProteinInterface

num_rbf = 16
num_rbf_bind = 16
num_rbf_rot = 3

number_pos_encoding = 16
nearest_neighbors = 30

min_dis = 0
max_dis = 20

min_dis_bind = 0
max_dis_bind = 100

min_dis_rot = 0
max_dis_rot = 6

# num_chi_angles = 5
num_side_distances = 10

# node_features = 20 + 6 + 3 + num_rbf_rot
# node_features = (
#     20
#     + 6
#     + (3 * num_side_distances)
#     + num_chi_angles
#     + (num_side_distances * num_rbf_rot)
# )
node_features = 20 + 6 + (3 * num_side_distances) + (num_side_distances * num_rbf_rot)
edge_features = 3 + 4 + num_rbf + number_pos_encoding


def valid_backbone(structure):
    atom_types = np.array(structure["atom_type"])

    atom_indices = np.logical_or(atom_types == "CA", atom_types == "N")
    atom_indices = np.logical_or(atom_types == "C", atom_indices)

    coordinates = np.array(structure["coordinates"])[atom_indices]

    if np.any(atom_types[atom_indices][0::3] != "N"):
        print(atom_types[atom_indices][0::3])

    if np.any(atom_types[atom_indices][1::3] != "CA"):
        print(atom_types[atom_indices][1::3])

    if np.any(atom_types[atom_indices][2::3] != "C"):
        print(atom_types[atom_indices][2::3])

    if len(coordinates) != 3 * len(np.unique(structure["residue_number"])):
        return False

    return True


def calc_dihedral_angles(protein_structure):
    # filter out non C CA and N atoms
    atom_types = np.array(protein_structure["atom_type"])

    atom_indices = np.logical_or(atom_types == "CA", atom_types == "N")
    atom_indices = np.logical_or(atom_types == "C", atom_indices)

    coordinates = np.array(protein_structure["coordinates"])[atom_indices]

    # Compute vector normals of planes defined by bonds
    bond_vectors = coordinates[1:] - coordinates[:-1]
    bond_vectors = bond_vectors / np.linalg.norm(bond_vectors, axis=1).reshape(
        (len(bond_vectors), 1)
    )

    n_1 = np.cross(bond_vectors[1:-1], bond_vectors[2:])
    n_2 = np.cross(bond_vectors[:-2], bond_vectors[1:-1])

    n_1 = n_1 / np.linalg.norm(n_1, axis=1).reshape(len(n_1), 1)
    n_2 = n_2 / np.linalg.norm(n_2, axis=1).reshape(len(n_2), 1)

    # Compute angle between the two planes
    dot = np.sum(n_1 * n_2, axis=1)
    # Clip for numerical stability
    # dot = np.clip(dot, 1e-6, 1 - 1e-6) original implementation bugged
    dot = np.clip(dot, -1 + 1e-6, 1 - 1e-6)  # Clipping may not be necessary but is used
    # in original Ingraham implementation

    angles = np.arccos(dot)
    # Get correct sign
    angles = np.sign(np.sum(bond_vectors[:-2] * n_1, axis=1)) * angles
    angles = np.pad(angles, (1, 2), mode="constant", constant_values=(0, 0))
    angles = angles.reshape((len(angles) // 3, 3))
    angles = np.concatenate((np.sin(angles), np.cos(angles)), 1)

    return angles


def get_nearest_neighbor_distances(protein_structure):
    # Find the k nearest neighbors for each residue and their distance
    atom_types = np.array(protein_structure["atom_type"])

    ca_indices = atom_types == "CA"

    ca_coords = np.array(protein_structure["coordinates"])[ca_indices]

    pairwise_distances = squareform(pdist(ca_coords))

    distances = np.zeros((len(ca_coords), nearest_neighbors))

    neighbor_indices = np.zeros((len(ca_coords), nearest_neighbors))

    for index, row in enumerate(pairwise_distances):
        current_indices = np.argpartition(row, nearest_neighbors)

        neighbor_indices[index] = current_indices[:nearest_neighbors]

        distances[index] = row[current_indices[:nearest_neighbors]]

    return distances, neighbor_indices


# def get_rotamer_locations_and_distances(protein_structure):
#     # get the location and distance of the sidechain centroid from each residue
#     atom_types = np.array(protein_structure["atom_type"])

#     ca_indices = atom_types == "CA"

#     ca_coords = np.array(protein_structure["coordinates"])[ca_indices]

#     atom_indices = np.logical_and(atom_types != "CA", atom_types != "N")
#     atom_indices = np.logical_and(atom_types != "C", atom_indices)
#     atom_indices = np.logical_and(atom_types != "O", atom_indices)

#     atom_coords = np.array(protein_structure["coordinates"])[atom_indices]

#     residue_side_chain_numbers = np.array(protein_structure["residue_number"])[
#         atom_indices
#     ]

#     residue_ca_numbers = np.array(protein_structure["residue_number"])[ca_indices]

#     rotamer_locations = []
#     rotamer_distances = []

#     for number in np.unique(residue_ca_numbers):
#         if len(atom_coords[residue_side_chain_numbers == number]) == 0:
#             rotamer_locations.append(ca_coords[list(residue_ca_numbers).index(number)])

#             rotamer_distances.append(0)

#             continue

#         side_chain_centroid = np.mean(
#             atom_coords[residue_side_chain_numbers == number], axis=0
#         )

#         ca_coord = ca_coords[list(residue_ca_numbers).index(number)]

#         rotamer_locations.append(side_chain_centroid)
#         rotamer_distances.append(np.linalg.norm(ca_coord - side_chain_centroid))

#     return rotamer_locations, rotamer_distances


def get_orientation_features(protein_structure, neighbor_indices, side_chain_locations):
    # Get orientation features for each node/edge corresponding to residues
    # in the protein
    atom_types = np.array(protein_structure["atom_type"])

    ca_indices = atom_types == "CA"

    ca_coords = np.array(protein_structure["coordinates"])[ca_indices]

    # Get pairwise vectors between adjancent alpha carbons
    virtual_bonds = ca_coords[1:] - ca_coords[:-1]

    b1 = virtual_bonds[:-1]
    b0 = virtual_bonds[1:]

    # Get norm of vectors before and after each residue
    n = np.cross(b1, b0)
    n = n / np.linalg.norm(n, axis=1).reshape(len(n), 1)

    # Get negative bisector
    o = b1 - b0
    o = o / np.linalg.norm(o, axis=1).reshape(len(o), 1)

    O = np.concatenate((o, n, np.cross(o, n)), axis=1)
    # Add 0s for the first and last residue
    O = np.pad(O, ((1, 1), (0, 0)), mode="constant", constant_values=0)

    neighbor_directions = np.zeros(neighbor_indices.shape + (3,))

    neighbor_orientations = np.zeros(neighbor_indices.shape + (4,))

    side_chain_directions = np.zeros((len(side_chain_locations), 10, 3))

    for residue_number, orientation in enumerate(O):
        # Get neighbor indicies
        adjacent_indices = neighbor_indices[residue_number].astype(int)

        # Calculate pairwise CA directions
        displacement = ca_coords[residue_number] - ca_coords[adjacent_indices]

        directions = np.matmul(orientation.reshape((3, 3)), displacement.T).T

        norm = np.linalg.norm(directions, axis=1).reshape(len(directions), 1)

        directions = np.divide(directions, norm, where=norm != 0)

        neighbor_directions[residue_number] = directions

        # Calculate rotamer centroid direction
        displacement = ca_coords[residue_number] - side_chain_locations[residue_number]

        directions = np.matmul(orientation.reshape((3, 3)), displacement.T).T

        norm = np.linalg.norm(directions * 1.0)

        directions = np.divide(directions, norm, where=norm != 0)
        # Reshape not necessary
        # directions = directions.reshape(directions.shape[0], -1)

        side_chain_directions[residue_number] = directions

        # Calculate relative orientation of coordinate systems
        neighbor_matricies = O[adjacent_indices].reshape((-1, 3, 3))

        rotation_matricies = np.matmul(
            orientation.reshape((3, 3)), np.transpose(neighbor_matricies, (0, 2, 1))
        )

        rotation_matricies = Rotation.from_dcm(rotation_matricies)

        rotation_matricies = rotation_matricies.as_quat()

        norm = np.linalg.norm(rotation_matricies, axis=1).reshape(
            len(rotation_matricies), 1
        )

        rotation_matricies = np.divide(rotation_matricies, norm, where=norm != 0)

        neighbor_orientations[residue_number] = rotation_matricies

    return neighbor_directions, neighbor_orientations, side_chain_directions


def rbf(distances, rotamer_distance=False, binding_site_distance=False):
    # Lift the input distances to a radial basis
    if not rotamer_distance and not binding_site_distance:
        min_dist = min_dis
        max_dist = max_dis
        counts = num_rbf

    elif rotamer_distance:
        min_dist = min_dis_rot
        max_dist = max_dis_rot
        counts = num_rbf_rot

    elif binding_site_distance:
        min_dist = min_dis_bind
        max_dist = max_dis_bind
        counts = num_rbf_bind

    means = np.linspace(min_dist, max_dist, counts)

    std = (max_dist - min_dist) / counts

    distances = np.repeat(
        np.expand_dims(distances, axis=len(distances.shape)),
        counts,
        len(distances.shape),
    )

    distances = np.exp(-(((distances - means) / std) ** 2))

    return distances


def positional_embedding(indices):
    # Performs a positional embedding for the edges of the input
    differences = indices - np.arange(len(indices)).reshape(len(indices), 1)

    result = np.exp(
        np.arange(0, number_pos_encoding, 2)
        * -1
        * (np.log(10000) / number_pos_encoding)
    )

    differences = np.repeat(
        np.expand_dims(differences, axis=len(differences.shape)),
        number_pos_encoding / 2,
        len(differences.shape),
    )

    result = differences * result
    result = np.concatenate((np.sin(result), np.cos(result)), 2)

    return result


def get_features(PDB_file, protein_chain_id):
    parser = PDB.PDBParser(QUIET=True)
    # parser = PDB.MMCIFParser()

    # Read in structure
    structure = parser.get_structure("inputer_protein", PDB_file)

    if not structure.get_list()[0].has_id(protein_chain_id):
        raise ValueError("The specified chain is not in the provided PDB file")

    protein_chain = structure.get_list()[0].__getitem__(str(protein_chain_id))

    # Store relevant pdb information in dictionary for easy access
    protein_residues = protein_chain.get_list()

    protein_information = {}
    protein_information["coordinates"] = []
    protein_information["residue_number"] = []
    protein_information["residue_type"] = []
    protein_information["atom_type"] = []
    protein_information["actual_number"] = []

    for index, res in enumerate(protein_residues):

        # Skip non standard residues
        if not PDB.Polypeptide.is_aa(res.get_resname(), standard=True):
            continue

        resname = res.get_resname()
        resnumber = index

        all_atoms = [atom.get_name().strip() for atom in res.get_atoms()]

        # Skip residues lacking a backbone antom
        if "C" not in all_atoms or "CA" not in all_atoms or "N" not in all_atoms:
            continue

        n_atom = []
        ca_atom = []
        c_atom = []

        for a_i, atom in enumerate(res.get_atoms()):
            if atom.get_name().strip() != "H":

                if atom.get_name().strip() == "N":
                    n_atom.append(atom.get_coord())
                    n_atom.append(resnumber)
                    n_atom.append(res.get_id()[1])
                    n_atom.append(resname)
                    n_atom.append(atom.get_name().strip())
                    continue

                elif atom.get_name().strip() == "CA":
                    ca_atom.append(atom.get_coord())
                    ca_atom.append(resnumber)
                    ca_atom.append(res.get_id()[1])
                    ca_atom.append(resname)
                    ca_atom.append(atom.get_name().strip())
                    continue

                elif atom.get_name().strip() == "C":
                    c_atom.append(atom.get_coord())
                    c_atom.append(resnumber)
                    c_atom.append(res.get_id()[1])
                    c_atom.append(resname)
                    c_atom.append(atom.get_name().strip())
                    continue

                protein_information["coordinates"].append(atom.get_coord())
                protein_information["residue_number"].append(resnumber)
                protein_information["actual_number"].append(res.get_id()[1])
                protein_information["residue_type"].append(resname)
                protein_information["atom_type"].append(atom.get_name().strip())

        protein_information["coordinates"].append(n_atom[0])
        protein_information["residue_number"].append(n_atom[1])
        protein_information["actual_number"].append(n_atom[2])
        protein_information["residue_type"].append(n_atom[3])
        protein_information["atom_type"].append(n_atom[4])

        protein_information["coordinates"].append(ca_atom[0])
        protein_information["residue_number"].append(ca_atom[1])
        protein_information["actual_number"].append(ca_atom[2])
        protein_information["residue_type"].append(ca_atom[3])
        protein_information["atom_type"].append(ca_atom[4])

        protein_information["coordinates"].append(c_atom[0])
        protein_information["residue_number"].append(c_atom[1])
        protein_information["actual_number"].append(c_atom[2])
        protein_information["residue_type"].append(c_atom[3])
        protein_information["atom_type"].append(c_atom[4])

    peptide_information = {}
    peptide_information["coordinates"] = []
    peptide_information["residue_number"] = []
    peptide_information["residue_type"] = []
    peptide_information["atom_type"] = []
    peptide_information["actual_number"] = []

    # Check that the backbones are valid for both molecules

    if not valid_backbone(protein_information):
        raise ValueError("The backbone of the specified chain is not well formed")
        return

    # Define array for node and edge features
    nodes = np.zeros(
        (len(np.unique(protein_information["residue_number"])), node_features)
    )
    edges = np.zeros(
        (
            len(np.unique(protein_information["residue_number"])),
            nearest_neighbors,
            edge_features,
        )
    )

    # Encode sequence of amino acids as node feature
    amino_acid_index = np.array(protein_information["atom_type"]) == "CA"

    amino_acid_sequence = np.array(protein_information["residue_type"])[
        amino_acid_index
    ]

    for index, amino_acid in enumerate(amino_acid_sequence):
        temp_target = np.zeros(20)
        temp_target[PDB.Polypeptide.d3_to_index[amino_acid]] = 1
        nodes[index, 0:20] = temp_target

    # Encode dihedral angles for amino acids
    angles = calc_dihedral_angles(protein_information)

    nodes[:, 20:26] = angles

    # Get distances and coordinates for orientation features
    neighbor_distances, neighbor_indices = get_nearest_neighbor_distances(
        protein_information
    )

    adjusted_neighbor_distances = rbf(neighbor_distances)

    edges[:, :, 0:num_rbf] = adjusted_neighbor_distances

    # Get rotamer distances and coordinates for orientation features
    # rotamer_locations, rotamer_distances = get_rotamer_locations_and_distances(
    # protein_information)
    # chi_angles = fetch_chi_angles(PDB_file)
    chain_dict_coords, chain_dict_distances = fetch_atom_distances(PDB_file)
    side_chain_distances = chain_dict_distances[protein_chain_id]
    # Don't need to convert to list
    # side_chain_coords = chain_dict_coords[protein_chain_id].tolist()
    side_chain_coords = chain_dict_coords[protein_chain_id]

    # Adjusted_rotamer_distances = rbf(np.array(rotamer_distances), True)
    adjusted_side_chain_distances = rbf(side_chain_distances, True)
    adjusted_side_chain_distances = adjusted_side_chain_distances.reshape(
        adjusted_side_chain_distances.shape[0], -1
    )

    # nodes[:, 26:26 + num_rbf_rot] = adjusted_rotamer_distances
    nodes[:, 26 : 26 + num_side_distances * num_rbf_rot] = adjusted_side_chain_distances

    # Get orientation features
    (
        neighbor_directions,
        neighbor_orientations,
        side_chain_directions,
    ) = get_orientation_features(
        protein_information, neighbor_indices, side_chain_coords
    )

    edges[:, :, num_rbf : num_rbf + 3] = neighbor_directions
    edges[:, :, num_rbf + 3 : num_rbf + 7] = neighbor_orientations

    side_chain_directions = side_chain_directions.reshape(
        side_chain_directions.shape[0], -1
    )

    nodes[
        :,
        26
        + num_side_distances * num_rbf_rot : 26
        + num_side_distances * num_rbf_rot
        + 3 * num_side_distances,
    ] = side_chain_directions

    # nodes[
    #     :, 26 + num_side_distances * num_rbf_rot + 3 * num_side_distances :
    # ] = chi_angles[protein_chain_id]

    edge_embeddings = positional_embedding(neighbor_indices)

    edges[:, :, num_rbf + 7 : num_rbf + 7 + number_pos_encoding] = edge_embeddings

    return nodes, edges, neighbor_indices, protein_information


def get_interface_orientation_feats(
    protein_structure_a, protein_structure_b, interface_indices, neighbor_indices
):
    """
    Modified version of Osama's orientation features to determine the direction
    and orientation information for the interface residues of A to corresponding
    neighbor residues on protein B.
    """
    # Get orientation features for each node/edge corresponding to residues
    # in the protein
    atom_types_a = np.array(protein_structure_a["atom_type"])
    ca_indices_a = atom_types_a == "CA"

    ca_coords_a = np.array(protein_structure_a["coordinates"])[ca_indices_a]
    # ca_coords_a = ca_coords_a[interface_indices]

    # Need to ge the CA atoms of the other protein as well
    atom_types_b = np.array(protein_structure_b["atom_type"])
    ca_indices_b = atom_types_b == "CA"

    ca_coords_b = np.array(protein_structure_b["coordinates"])[ca_indices_b]

    # Get pairwise vectors between adjancent alpha carbons
    virtual_bonds_a = ca_coords_a[1:] - ca_coords_a[:-1]

    b1_a = virtual_bonds_a[:-1]
    b0_a = virtual_bonds_a[1:]

    # Get norm of vectors before and after each residue
    n_a = np.cross(b1_a, b0_a)
    n_a = n_a / np.linalg.norm(n_a, axis=1).reshape(len(n_a), 1)

    # Get negative bisector
    o_a = b1_a - b0_a
    o_a = o_a / np.linalg.norm(o_a, axis=1).reshape(len(o_a), 1)
    O_a = np.concatenate((o_a, n_a, np.cross(o_a, n_a)), axis=1)

    # Add 0s for the first and last residue
    O_a = np.pad(O_a, ((1, 1), (0, 0)), mode="constant", constant_values=0)

    # Do the same for B
    virtual_bonds_b = ca_coords_b[1:] - ca_coords_b[:-1]

    b1_b = virtual_bonds_b[:-1]
    b0_b = virtual_bonds_b[1:]

    # Get norm of vectors before and after each residue
    n_b = np.cross(b1_b, b0_b)
    n_b = n_b / np.linalg.norm(n_b, axis=1).reshape(len(n_b), 1)

    # Get negative bisector
    o_b = b1_b - b0_b
    o_b = o_b / np.linalg.norm(o_b, axis=1).reshape(len(o_b), 1)
    O_b = np.concatenate((o_b, n_b, np.cross(o_b, n_b)), axis=1)

    # Add 0s for the first and last residue
    O_b = np.pad(O_b, ((1, 1), (0, 0)), mode="constant", constant_values=0)

    interface_directions = np.zeros(neighbor_indices.shape + (3,))
    interface_orientations = np.zeros(neighbor_indices.shape + (4,))

    O_a_int = O_a[interface_indices]
    ca_coords_a = ca_coords_a[interface_indices]

    for residue_number, orientation in enumerate(O_a_int):
        # Get neighbor indicies
        # adjacent_indices = neighbor_indices[residue_number].astype(int)
        adjacent_indices = neighbor_indices[residue_number]

        # Calculate pairwise CA directions
        displacement = ca_coords_a[residue_number] - ca_coords_b[adjacent_indices]

        directions = np.matmul(orientation.reshape((3, 3)), displacement.T).T

        norm = np.linalg.norm(directions, axis=1).reshape(len(directions), 1)

        directions = np.divide(directions, norm, where=norm != 0)

        interface_directions[residue_number] = directions

        # Calculate relative orientation of coordinate systems
        neighbor_matricies = O_b[adjacent_indices].reshape((-1, 3, 3))

        rotation_matricies = np.matmul(
            orientation.reshape((3, 3)), np.transpose(neighbor_matricies, (0, 2, 1))
        )
        rotation_matricies = Rotation.from_dcm(rotation_matricies)
        rotation_matricies = rotation_matricies.as_quat()

        norm = np.linalg.norm(rotation_matricies, axis=1).reshape(
            len(rotation_matricies), 1
        )

        rotation_matricies = np.divide(rotation_matricies, norm, where=norm != 0)

        interface_orientations[residue_number] = rotation_matricies

    return interface_directions, interface_orientations


def get_interface(PDB_file, prot_info_a, prot_info_b, pair):
    """
    Get the protein interface indices, and build edges
    """
    pi = ProteinInterface(PDB_file)

    results = pi.build()

    indices_a, indices_b, neighbors_a, neighbors_b, distances_a, distances_b = results[
        pair
    ]

    num_features = 3 + 4 + num_rbf

    interface_edges_a = np.zeros(
        (indices_a.shape[0], neighbors_a.shape[1], num_features)
    )
    interface_edges_b = np.zeros(
        (indices_b.shape[0], neighbors_b.shape[1], num_features)
    )

    adjusted_neighbor_distances_a = rbf(distances_a)
    adjusted_neighbor_distances_b = rbf(distances_b)

    interface_edges_a[:, :, 0:num_rbf] = adjusted_neighbor_distances_a
    interface_edges_b[:, :, 0:num_rbf] = adjusted_neighbor_distances_b

    neighbor_directions_a, neighbor_orientations_a = get_interface_orientation_feats(
        prot_info_a, prot_info_b, indices_a, neighbors_a
    )

    interface_edges_a[:, :, num_rbf : num_rbf + 3] = neighbor_directions_a
    interface_edges_a[:, :, num_rbf + 3 : num_rbf + 7] = neighbor_orientations_a

    # Need to switch prot_info_a and prot_info_b as well to get edges B->A
    neighbor_directions_b, neighbor_orientations_b = get_interface_orientation_feats(
        prot_info_b, prot_info_a, indices_b, neighbors_b
    )

    interface_edges_b[:, :, num_rbf : num_rbf + 3] = neighbor_directions_b
    interface_edges_b[:, :, num_rbf + 3 : num_rbf + 7] = neighbor_orientations_b

    return (
        indices_a,
        indices_b,
        neighbors_a,
        neighbors_b,
        interface_edges_a,
        interface_edges_b,
    )
