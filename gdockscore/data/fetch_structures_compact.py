"""
Download files from the PDB and then use the get_features function to store the
appropriate graoh representation of the protein in a compressed npz file for the
creation of a DataSet class for use with PyTorch.

TODO: Improve efficiency.
"""

from gdockscore.data.compute_features import get_features, get_interface
import pandas as pd
import numpy as np
import os
import argparse
import sys
import logging

logging.basicConfig(filename="fetch_error.log", level=logging.ERROR)


def get_chain_ids(biounit):
    """Get chain ids of a biounit"""
    # Intialize an empty list of chain ids
    chain_ids = []
    chains = biounit.get_chains()

    for chain in chains:
        chain_ids.append(chain.get_id())

    return chain_ids


def create_structures(db_path, path_files, path_structures):
    """
    Fetch each chains nodes, edges, and neighbor matrices and every pair of
    chains interface edges
    """
    db = pd.read_csv(db_path, sep=",")

    try:
        os.mkdir(path_structures)

    except OSError:
        logging.error("Files folder already exists")

    # Generate list of files in current wd
    db_files = db["File"].values
    db_files = set(db_files)
    # bad_files = []

    for dirpath, subdirs, files in os.walk(path_files):
        for file in files:
            if file not in db_files:
                continue

            pairs = db[db["File"] == file].values[:, 1:]
            pdb_path = os.path.join(dirpath, file)
            completed_chains = []
            prot_information = {}

            file_data = {}

            for pair in pairs:
                try:
                    # global bad_pair
                    bad_pair = pair
                    for chain in pair:
                        if chain not in completed_chains:
                            nodes, edges, neighbors, prot_info = get_features(
                                pdb_path, chain
                            )

                            node_key = "nodes_" + chain
                            edge_key = "edges_" + chain
                            neighbors_key = "neighbors_" + chain

                            file_data[node_key] = nodes
                            file_data[edge_key] = edges
                            file_data[neighbors_key] = neighbors

                            completed_chains.append(chain)

                            prot_information[chain] = prot_info

                    # May not be necessary to convert list to tuple here
                    pair = tuple(pair)
                    pair_name = pair[0] + pair[1]
                    prot_info_1 = prot_information[pair[0]]
                    prot_info_2 = prot_information[pair[1]]

                    (
                        interface_1_indices,
                        interface_2_indices,
                        neighbors_interface_1,
                        neighbors_interface_2,
                        int_edges_1to2,
                        int_edges_2to1,
                    ) = get_interface(pdb_path, prot_info_1, prot_info_2, pair)

                    dir_1 = pair[0] + pair[1]
                    dir_2 = pair[1] + pair[0]

                    interface_1_indices_key = pair_name + "_int_" + pair[0] + "_indices"
                    interface_2_indices_key = pair_name + "_int_" + pair[1] + "_indices"
                    interface_1_neighbors_key = (
                        pair_name + "_int_" + pair[0] + "_neighbors"
                    )
                    interface_2_neighbors_key = (
                        pair_name + "_int_" + pair[1] + "_neighbors"
                    )
                    interface_edges_1_to_2_key = pair_name + "_int_" + dir_1 + "_edges"
                    interface_edges_2_to_1_key = pair_name + "_int_" + dir_2 + "_edges"

                    file_data[interface_1_indices_key] = interface_1_indices
                    file_data[interface_2_indices_key] = interface_2_indices
                    file_data[interface_1_neighbors_key] = neighbors_interface_1
                    file_data[interface_2_neighbors_key] = neighbors_interface_2
                    file_data[interface_edges_1_to_2_key] = int_edges_1to2
                    file_data[interface_edges_2_to_1_key] = int_edges_2to1

                # IndexError handles possibility of a messed up chain with less than
                # 30 residues or a case where number of residues is less than k for
                # the interface code
                except Exception as error:
                    logging.error(
                        "Data point "
                        + str(file)
                        + " "
                        + str(bad_pair)
                        + " failed with error: "
                        + str(error)
                    )

                    # Probably a more efficient way to do this
                    drop_index = db[
                        (
                            (db.File == file)
                            & (db.Chain1 == bad_pair[0])
                            & (db.Chain2 == bad_pair[1])
                        )
                    ].index.tolist()

                    db.drop(drop_index, inplace=True)

                    # bad_files_entry = [file, bad_pair[0], bad_pair[1]]
                    # bad_files.append(bad_files_entry)

                    continue

            # This makes sure that interface edges are present. If no interface
            # is found then there would only be 6 entries which are the nodes,
            # edges, and neighbors of two chains
            try:
                assert len(file_data) > 6, "The file does not contain an interface"

                save_path = path_structures + "/" + file
                np.savez_compressed(save_path, **file_data)

            except AssertionError as error:
                try:
                    drop_index = db[(db.File == file)].index.tolist()
                    db.drop(drop_index, inplace=True)

                    # bad_files_entry = [file, bad_pair[0], bad_pair[1]]
                    # bad_files.append(bad_files_entry)

                except Exception as error:
                    logging.error("The entry has already been removed from the db")

    # Create updated database
    num = np.random.randint(1, high=1000)
    file_name = "successful_files_" + str(num) + ".csv"
    db.to_csv(file_name, sep=",", index=False)

    # Also create a record of failed files
    # bad_data = zip(bad_files)
    # bad_db = pd.DataFrame(bad_data, columns=["File", "Chain1", "Chain2"])
    # bad_file_name = "bad_files_" + str(num) + ".csv"
    # bad_db.to_csv(bad_file_name, sep=",", index=False)

    return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Set parser arguments
    parser.add_argument(
        "--db_path",
        dest="db_path",
        required=True,
        type=str,
        help="The location of the database file",
    )
    parser.add_argument(
        "--pdbs_path",
        dest="pdbs_path",
        required=True,
        type=str,
        help="The location of the PDB file",
    )
    parser.add_argument(
        "--structures_path",
        dest="target_path",
        required=True,
        type=str,
        help="The location to save the .npz files",
    )

    args = parser.parse_args()

    pdbs_path = args.pdbs_path
    target_path = args.target_path
    db_path = args.db_path

    create_structures(db_path, pdbs_path, target_path)
