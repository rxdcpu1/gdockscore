"""
Functions that will find interacting chains in a PDB file, find the appropriate
interface residues and their indices as well as the indices of all of the residues
in the other protein chain that it interacts with.

TODO: The efficiency of this code could be improved by creating arrays of appropriate
size using np.zeros and then updating each row accordingly. I also need to update this
to only store the dense distance matrix and convert it to sparse matrix as needed
instead of storing both to increase memory efficiency and reducing number of computations.
"""

from Bio import PDB
import numpy as np
from scipy import spatial
import argparse
import os
import itertools

# import logging

# logging.basicConfig(filename="interface_error.log", level=logging.ERROR)


class ProteinInterface:
    """Finds the interface structure between to polypeptide chains

    This class will contain the appropriate methods to find interacting chains
    in a PDB and then find the appropriate interface residues in each PDB file
    including their indices as well as the indices of each interface residue it
    interacts with in the other protein chain

    Attributes
    ----------
    pdb_path : str
        Path to a PDB file
    k : int
        Number of nearest neighbors
    num_interacting : int
        Number of interacting residues
    thresh : int
        Threshold distance between residues to be considered interacting
    save_dir : str
        Path to save files

    Methods
    -------
    fetch_chain(pdb_file, chain)
        Grabs a chain from a PDB file that has been parsed
    find_chain_pairs()
        Finds interacting chains in the file
    find_neighbor_indices_and_distances(interface_distance_matrix)
       Find neighboring residues and indices given an interface distance matrix
    find_interface_information(sparse_distance_matrix, distance_matrix)
       Given the sparse and non-sparse distance matrix between two polypeptide chains
       find the interface structure
    build():
       Find the interface information for all chain pairs for the given PDB file
    """

    def __init__(
        self, pdb_path, k=10, num_interacting=1, thresh=8, save_dir=os.getcwd()
    ):
        self.parser = PDB.PDBParser(QUIET=True)
        self.pdb_path = pdb_path
        # Number of neighbors for interface residues
        self.k = k
        # Parameters for defining interaction ie. number of interacting residues
        # and how close they need to be to considered interacting
        self.num_interacting = num_interacting
        self.thresh = thresh
        self.save_dir = save_dir

        try:
            self.structure = self.parser.get_structure("input", self.pdb_path)
            self.structure = self.structure[0]

        except OSError:
            print("The specified PDB file does not exist.")

    def get_atoms(self, chain_a, chain_b):
        """Get alpha carbon atoms in a polypeptide chain"""
        atoms_a = []
        atoms_b = []

        for res_a in chain_a:
            # Skip non-standard amino acids
            if not PDB.Polypeptide.is_aa(res_a.get_resname(), standard=True):
                continue

            try:
                atom_a = res_a["CA"].get_coord()
                atoms_a.append(atom_a)
            except KeyError:
                continue

        for res_b in chain_b:
            if not PDB.Polypeptide.is_aa(res_b.get_resname(), standard=True):
                continue

            try:
                atom_b = res_b["CA"].get_coord()
                atoms_b.append(atom_b)
            except KeyError:
                continue

        assert (
            len(atoms_a) > 0 and len(atoms_b) > 0
        ), "The selected chains do not contain any atoms. Please check the input PDB file..."

        atoms_a = np.array(atoms_a)
        atoms_b = np.array(atoms_b)

        return atoms_a, atoms_b

    def find_chain_pairs(self):
        """Find all interacting chain pairs in a PDB"""
        chains = self.structure.get_chains()
        chains_list = []

        for chain in chains:
            chains_list.append(chain.id)

        combos = itertools.combinations(chains_list, 2)
        interacting_pairs = []
        distance_matrices = []
        sparse_distance_matrices = []

        for combo in combos:
            chain_a = self.structure[combo[0]]
            chain_b = self.structure[combo[1]]

            try:
                atoms_a, atoms_b = self.get_atoms(chain_a, chain_b)
            except AssertionError:
                # logging.error("At least 1 chain is empty. Skipping this combo.")
                continue

            # Construct the cKDTrees and find sparse distance matrix
            tree_a = spatial.cKDTree(atoms_a)
            tree_b = spatial.cKDTree(atoms_b)

            sparse_distance = tree_a.sparse_distance_matrix(tree_b, self.thresh, p=2.0)
            sparse_distance = sparse_distance.toarray()

            # len(combo) == 1 accounts for the negative datasets imperfect
            # docking (there may be no residues that close)
            if np.count_nonzero(sparse_distance) >= self.num_interacting:
                distance_matrix = spatial.distance_matrix(atoms_a, atoms_b)

                interacting_pairs.append(combo)
                distance_matrices.append(distance_matrix)
                sparse_distance_matrices.append(sparse_distance)

            else:
                continue

        assert (
            len(interacting_pairs) > 0
        ), "There appear to be no interacting chains in your PDB file. Please check your input PDB file..."

        # This can be removed
        # self.interacting_pairs = interacting_pairs

        return interacting_pairs, distance_matrices, sparse_distance_matrices

    def find_neighbor_indices_and_distances(self, interface_distance_matrix):
        """Find atom neighbor indices and distances for interface residues"""
        neighbor_indices = []
        neighbor_distances = []

        for i in range(interface_distance_matrix.shape[0]):
            current = interface_distance_matrix[i]

            indices = np.argpartition(current, self.k)
            indices = indices[: self.k]

            indices = indices.tolist()
            neighbor_indices.append(indices)

            distances = current[indices].tolist()
            neighbor_distances.append(distances)

        neighbor_indices = np.array(neighbor_indices)
        neighbor_distances = np.array(neighbor_distances)

        return neighbor_indices, neighbor_distances

    def find_interface_information(self, sparse_distance_matrix, distance_matrix):
        """
        Finds rows in the interacting pairs that have nonzero elements. This
        means that for the residue in A, it is within the threshold distance of
        a residue in protein B.
        """
        non_zero_a = np.count_nonzero(sparse_distance_matrix, axis=1)
        indices_a = np.nonzero(non_zero_a)
        indices_a = np.array(indices_a[0])

        # To find the residues in other protein, each column is checked for the
        # number of nonzero values since each column represents protein B's
        # residues interactions with residues in A
        non_zero_b = np.count_nonzero(sparse_distance_matrix, axis=0)
        indices_b = np.nonzero(non_zero_b)
        indices_b = np.array(indices_b[0])

        interface_a = distance_matrix[indices_a]
        neighbors_a, distances_a = self.find_neighbor_indices_and_distances(interface_a)

        # Need to transpose the distance matrix to find the neighbors for b
        distance_matrix_trans = distance_matrix.T

        interface_b = distance_matrix_trans[indices_b]
        neighbors_b, distances_b = self.find_neighbor_indices_and_distances(interface_b)

        result = [
            indices_a,
            indices_b,
            neighbors_a,
            neighbors_b,
            distances_a,
            distances_b,
        ]

        return result

    def build(self, save=False):
        """Find all interface information"""
        chain_pairs, dist_mat, sparse_dist_mat = self.find_chain_pairs()

        results = {}

        for i, pair in enumerate(chain_pairs):
            result = self.find_interface_information(sparse_dist_mat[i], dist_mat[i])

            # Update this to be more clean
            indices_a = result[0]
            indices_b = result[1]

            neighbors_a = result[2]
            neighbors_b = result[3]

            distances_a = result[4]
            distances_b = result[5]

            if save is True:
                int_a_ind_path = os.path.join(self.save_dir, "indices_" + pair[0])
                int_b_ind_path = os.path.join(self.save_dir, "indices_" + pair[1])

                int_a_neigh_path = os.path.join(self.save_dir, "neighbors_" + pair[0])
                int_b_neigh_path = os.path.join(self.save_dir, "neighbors_" + pair[1])

                int_a_neigh_dist_path = os.path.join(
                    self.save_dir, "neighbor_distances_" + pair[0]
                )
                int_b_neigh_dist_path = os.path.join(
                    self.save_dir, "neighbor_distances_" + pair[1]
                )

                np.savez_compressed(int_a_ind_path, indices_a)
                np.savez_compressed(int_b_ind_path, indices_b)

                np.savez_compressed(int_a_neigh_path, neighbors_a)
                np.savez_compressed(int_b_neigh_path, neighbors_b)

                np.savez_compressed(int_a_neigh_dist_path, distances_a)
                np.savez_compressed(int_b_neigh_dist_path, distances_b)

            result = [
                indices_a,
                indices_b,
                neighbors_a,
                neighbors_b,
                distances_a,
                distances_b,
            ]

            results[pair] = result

        return results


if __name__ == "__main__":
    # For testing purposes
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pdb_file",
        dest="pdb_file",
        type=str,
        required=True,
        help="The path to the pdb file for calculations",
    )

    args = parser.parse_args()

    pdb_path = args.pdb_file

    pi = ProteinInterface(pdb_path)
    pi.build()
