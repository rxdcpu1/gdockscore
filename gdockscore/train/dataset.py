"""
Description: Using the Dataset and DataLoader classes from PyTorch to take the generated
structures for each protein and produce a data set that can be used with the DataLoader
class to train the proposed network.

TODO: Add padding to each example to implement batch sizes > 1
"""

from __future__ import print_function, division
import os
import pandas as pd
import numpy as np
import torch
from torch.utils.data import Dataset


class StructuresDataset(Dataset):
    """
    Provide the db file path, the directory where the structures are, and the
    location of the CSV file containing every file and its file path for fast
    lookup
    """

    def __init__(self, file_path, root_dir, location_path, transform=None):
        self.chain_pairs = pd.read_csv(file_path, header=0)
        self.root_dir = root_dir
        self.locations = pd.read_csv(location_path, header=0)
        self.transform = transform

    def __len__(self):
        return len(self.chain_pairs)

    def __getitem__(self, idx):
        # Check if input indices are a tensor and convert
        # to a list as necessary
        if torch.is_tensor(idx):
            idx = idx.tolist()

        # Fetch the two chains that need to be inputed into the network from the
        # database file
        struct_file = self.chain_pairs.iloc[idx, 0]
        struct_chain_1 = self.chain_pairs.iloc[idx, 1]
        struct_chain_2 = self.chain_pairs.iloc[idx, 2]
        label = self.chain_pairs.iloc[idx, 3]

        # Fetch the appropriate structure files including
        # nodes, edges, and neighbours (these are stored in
        # npz files
        # for dir_path, subdirs, files in os.walk(self.root_dir):
        #  for file in files:
        #       if file.startswith(struct_file) is True:
        #          correct_path = dir_path
        #          break

        # if label == 1:
        #    correct_path = os.path.join(self.root_dir, "positive",
        #                                struct_file + ".npz")
        # else:
        #    correct_path = os.path.join(self.root_dir, "negative",
        #                                struct_file + ".npz")

        correct_path = self.locations[
            self.locations["File"] == struct_file
        ].values.tolist()[0][1]

        correct_path = self.root_dir + correct_path

        with np.load(correct_path) as data:

            edges_1_key = "edges_" + struct_chain_1
            nodes_1_key = "nodes_" + struct_chain_1
            neighbors_1_key = "neighbors_" + struct_chain_1
            int_1_indices_key = (
                struct_chain_1 + struct_chain_2 + "_int_" + struct_chain_1 + "_indices"
            )
            int_1_neighbors_key = (
                struct_chain_1
                + struct_chain_2
                + "_int_"
                + struct_chain_1
                + "_neighbors"
            )
            int_1_to_2_edges_key = (
                struct_chain_1
                + struct_chain_2
                + "_int_"
                + struct_chain_1
                + struct_chain_2
                + "_edges"
            )

            edges_2_key = "edges_" + struct_chain_2
            nodes_2_key = "nodes_" + struct_chain_2
            neighbors_2_key = "neighbors_" + struct_chain_2
            int_2_indices_key = (
                struct_chain_1 + struct_chain_2 + "_int_" + struct_chain_2 + "_indices"
            )
            int_2_neighbors_key = (
                struct_chain_1
                + struct_chain_2
                + "_int_"
                + struct_chain_2
                + "_neighbors"
            )
            int_2_to_1_edges_key = (
                struct_chain_1
                + struct_chain_2
                + "_int_"
                + struct_chain_2
                + struct_chain_1
                + "_edges"
            )

            edges_1 = data[edges_1_key]
            nodes_1 = data[nodes_1_key]
            neighbors_1 = data[neighbors_1_key]
            int_indices_1 = data[int_1_indices_key]
            int_1_neighbors = data[int_1_neighbors_key]
            int_1_to_2_edges = data[int_1_to_2_edges_key]

            edges_2 = data[edges_2_key]
            nodes_2 = data[nodes_2_key]
            neighbors_2 = data[neighbors_2_key]
            int_indices_2 = data[int_2_indices_key]
            int_2_neighbors = data[int_2_neighbors_key]
            int_2_to_1_edges = data[int_2_to_1_edges_key]

        # Combine the 6 components of the structure of a protein chain
        # into a list
        final_1 = [
            edges_1,
            nodes_1,
            neighbors_1,
            int_indices_1,
            int_1_neighbors,
            int_1_to_2_edges,
        ]
        final_2 = [
            edges_2,
            nodes_2,
            neighbors_2,
            int_indices_2,
            int_2_neighbors,
            int_2_to_1_edges,
        ]

        # Transform the numpy array objects to tensors for use with PyTorch
        if self.transform is not None:
            final_1 = self.transform(final_1)
            final_2 = self.transform(final_2)

        sample = {
            "Protein_1": final_1,
            "Protein_2": final_2,
            "Label": label,
        }

        return sample


class ToTensor:
    def __call__(self, sample):
        nodes, edges, neighbors, int_indices, int_neighbors, int_edges = (
            sample[1],
            sample[0],
            sample[2],
            sample[3],
            sample[4],
            sample[5],
        )

        nodes = torch.squeeze(torch.from_numpy(nodes))
        edges = torch.squeeze(torch.from_numpy(edges))
        neighbors = torch.squeeze(torch.from_numpy(neighbors).long())
        int_indices = torch.squeeze(torch.from_numpy(int_indices).long())
        int_neighbors = torch.squeeze(torch.from_numpy(int_neighbors).long())
        int_edges = torch.squeeze(torch.from_numpy(int_edges))

        return {
            "nodes": nodes,
            "edges": edges,
            "neighbors": neighbors,
            "int_indices": int_indices,
            "int_neighbors": int_neighbors,
            "int_edges": int_edges,
        }
