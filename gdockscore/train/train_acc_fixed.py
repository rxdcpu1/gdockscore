"""Train the model using the specified dataset."""

import torch
import torch.nn as nn
import torch.utils as utils
import torch.optim as optim
from dataset import StructuresDataset, ToTensor
import math
import glob

# import sys

# sys.path.insert(0, "/home/kimlab5/mmcfee/gdockscore/gdockscore/models")
from gdockscore.models import FullModel

# from models import FullModel

import gc

device = torch.device("cuda:0")

# net = FullModel(39, 86, 23, 6, 64, 6, 64, 128, 64, dropout=0.2)
# net = FullModel(39, 86, 23, 12, 124, 12, 124, 256, 124, dropout=0.2)
# net = FullModel(39, 86, 23, 6, 64, 6, 64, 128, 64, dropout=0.1)
# net = FullModel(39, 86, 23, 3, 128, 3, 128, 256, 128, dropout=0.3)
# net = FullModel(39, 86, 23, 3, 32, 3, 32, 64, 32, dropout=0.2) # current working model
# net = FullModel(39, 86, 23, 1, 8, 1, 8, 16, 8, dropout=0.2)
# net = FullModel(39, 86, 23, 3, 8, 3, 8, 16, 8, dropout=0.2)
# net = FullModel(39, 86, 23, 2, 8, 2, 8, 16, 8, dropout=0.2)
net = FullModel(39, 86, 23, 3, 16, 3, 16, 32, 16, dropout=0.2)

net = net.double()
net = net.to(device)

# criterion = nn.BCELoss()
criterion = nn.BCEWithLogitsLoss()
# optimizer = optim.Adam(net.parameters(), lr=0.0001)
optimizer = optim.Adam(net.parameters(), lr=0.0001)

sig = nn.Sigmoid()

try:
    weights = glob.glob("*weights*.pth")[0]
    checkpoint = torch.load(weights)
    net.load_state_dict(checkpoint)
    # net.load_state_dict(checkpoint["model_state_dict"])
    # optimizer.load_state_dict(checkpoint["optimizer_state_dict"])

except Exception as e:
    print(e)
    print(
        "There is a problem with the checkpoint file or no weights are available.",
        flush=True,
    )

#train_dataset = StructuresDataset(
#    "/home/kimlab5/mmcfee/gdockscore/datasets/db_v3_3.0/final_mmseq2_shuffle/train_df.csv",
#    "/home/kimlab5/mmcfee/project_1_data/dataset_v3_3.0/",
#    "/home/kimlab5/mmcfee/gdockscore/datasets/db_v3_3.0/final_mmseq2_shuffle/fp.csv",
#    ToTensor(),
#)

#test_dataset = StructuresDataset(
#    "/home/kimlab5/mmcfee/gdockscore/datasets/db_v3_3.0/final_mmseq2_shuffle/val_df.csv",
#    "/home/kimlab5/mmcfee/project_1_data/dataset_v3_3.0/",
#    "/home/kimlab5/mmcfee/gdockscore/datasets/db_v3_3.0/final_mmseq2_shuffle/fp.csv",
#    ToTensor(),
#)

train_dataset = StructuresDataset(
    "/home/kimlab5/mmcfee/gdockscore/datasets/deeprank/deeprank_train.csv",
    "/home/kimlab5/mmcfee/project_1_data/deeprank/exp2_scoring/bm5/",
    "/home/kimlab5/mmcfee/gdockscore/datasets/deeprank/fp.csv",
    ToTensor(),
)

test_dataset = StructuresDataset(
    "/home/kimlab5/mmcfee/gdockscore/datasets/deeprank/deeprank_val.csv",
    "/home/kimlab5/mmcfee/project_1_data/deeprank/exp2_scoring/bm5/",
    "/home/kimlab5/mmcfee/gdockscore/datasets/deeprank/fp.csv",
    ToTensor(),
)

# dataset_len = dataset.__len__()
# train_len = int(0.8 * dataset_len)
# test_len = dataset_len - train_len

# trainset, testset = utils.data.random_split(dataset, [train_len, test_len])

train_loader = utils.data.DataLoader(train_dataset, shuffle=True, batch_size=1)
test_loader = utils.data.DataLoader(test_dataset, shuffle=True, batch_size=1)
train_loader_2 = utils.data.DataLoader(train_dataset, shuffle=True, batch_size=1)

train_loss = []
test_loss = []
train_accuracy = []
test_accuracy = []
train_hits = []
test_hits = []


def train(num_epochs, epsilon=0.0001):
    # Necessary values to keep track of early stopping
    conv_i = 1
    current_best_avg = math.inf

    # Get base line model performance
    current_loss_test = []
    current_loss_train = []
    current_accuracy_test = []
    current_accuracy_train = []
    current_hit_rate_test = 0
    current_total_hits_test = 0
    current_hit_rate_train = 0
    current_total_hits_train = 0

    # Early stopping counter
    stop_counter = 0

    net.eval()
    with torch.no_grad():
        for ii, data in enumerate(train_loader_2, 1):
            gc.collect()
            # torch.cuda.empty_cache()

            if ii >= 5000:
                break

            try:
                protein_1 = data["Protein_1"]
                protein_2 = data["Protein_2"]
                labels = data["Label"].double().to(device)

                nodes = protein_1["nodes"].to(device)
                edges = protein_1["edges"].to(device)
                neighbors = protein_1["neighbors"].to(device)
                nodes_2 = protein_2["nodes"].to(device)
                edges_2 = protein_2["edges"].to(device)
                neighbors_2 = protein_2["neighbors"].to(device)
                int_idx = protein_1["int_indices"].to(device)
                int_edges = protein_1["int_edges"].to(device)
                int_neighbors = protein_1["int_neighbors"].to(device)
                int_idx_2 = protein_2["int_indices"].to(device)
                int_edges_2 = protein_2["int_edges"].to(device)
                int_neighbors_2 = protein_2["int_neighbors"].to(device)

                if len(int_idx.shape) == 1:
                    int_idx = torch.unsqueeze(int_idx, 0)
                    int_edges = torch.unsqueeze(int_edges, 0)
                    int_neighbors = torch.unsqueeze(int_neighbors, 0)
                if len(int_idx_2.shape) == 1:
                    int_idx_2 = torch.unsqueeze(int_idx_2, 0)
                    int_edges_2 = torch.unsqueeze(int_edges_2, 0)
                    int_neighbors_2 = torch.unsqueeze(int_neighbors_2, 0)

                outputs = net(
                    nodes,
                    edges,
                    neighbors,
                    nodes_2,
                    edges_2,
                    neighbors_2,
                    int_idx,
                    int_edges,
                    int_neighbors,
                    int_idx_2,
                    int_edges_2,
                    int_neighbors_2,
                )

                outputs = outputs.reshape(-1).double()

                loss = criterion(outputs, labels)

                current_loss_train.append(loss.item())

                # Calculate hit rate
                if sig(outputs).item() >= 0.9:
                    current_total_hits_train += 1
                    if int(labels.item()) == 1:
                        current_hit_rate_train += 1

                # Calculate accuracy
                if sig(outputs) >= 0.5:
                    prediction = 1
                else:
                    prediction = 0

                if prediction == int(labels.item()):
                    current_accuracy_train.append(1)
                else:
                    current_accuracy_train.append(0)

            except Exception as e:
                with open("errors.txt", "a") as error_file:
                    error_file.write(str(e) + "\n")
                    torch.cuda.empty_cache()

        train_loss.append(sum(current_loss_train) / len(current_loss_train))
        train_accuracy.append(sum(current_accuracy_train) / len(current_accuracy_train))
        if current_total_hits_train > 0:
            train_hits.append(current_hit_rate_train / current_total_hits_train)
        else:
            train_hits.append(0)

        with open("train_loss.txt", "a") as train_loss_file:
            train_loss_file.write(str(train_loss[-1]) + "\n")
        with open("train_accuracy.txt", "a") as train_accuracy_file:
            train_accuracy_file.write(str(train_accuracy[-1]) + "\n")
        with open("train_hits.txt", "a") as train_hit_file:
            train_hit_file.write(str(train_hits[-1]) + "\n")

        for iii, data in enumerate(test_loader, 1):
            gc.collect()
            # torch.cuda.empty_cache()

            if iii >= 5000:
                break

            try:
                protein_1 = data["Protein_1"]
                protein_2 = data["Protein_2"]
                labels = data["Label"].double().to(device)

                nodes = protein_1["nodes"].to(device)
                edges = protein_1["edges"].to(device)
                neighbors = protein_1["neighbors"].to(device)
                nodes_2 = protein_2["nodes"].to(device)
                edges_2 = protein_2["edges"].to(device)
                neighbors_2 = protein_2["neighbors"].to(device)
                int_idx = protein_1["int_indices"].to(device)
                int_edges = protein_1["int_edges"].to(device)
                int_neighbors = protein_1["int_neighbors"].to(device)
                int_idx_2 = protein_2["int_indices"].to(device)
                int_edges_2 = protein_2["int_edges"].to(device)
                int_neighbors_2 = protein_2["int_neighbors"].to(device)

                if len(int_idx.shape) == 1:
                    int_idx = torch.unsqueeze(int_idx, 0)
                    int_edges = torch.unsqueeze(int_edges, 0)
                    int_neighbors = torch.unsqueeze(int_neighbors, 0)
                if len(int_idx_2.shape) == 1:
                    int_idx_2 = torch.unsqueeze(int_idx_2, 0)
                    int_edges_2 = torch.unsqueeze(int_edges_2, 0)
                    int_neighbors_2 = torch.unsqueeze(int_neighbors_2, 0)

                outputs = net(
                    nodes,
                    edges,
                    neighbors,
                    nodes_2,
                    edges_2,
                    neighbors_2,
                    int_idx,
                    int_edges,
                    int_neighbors,
                    int_idx_2,
                    int_edges_2,
                    int_neighbors_2,
                )

                outputs = outputs.reshape(-1).double()

                loss = criterion(outputs, labels)

                current_loss_test.append(loss.item())

                # Calculate hit rate
                if sig(outputs).item() >= 0.9:
                    current_total_hits_test += 1
                    if int(labels.item()) == 1:
                        current_hit_rate_test += 1

                if sig(outputs) >= 0.5:
                    prediction = 1
                else:
                    prediction = 0

                if prediction == int(labels.item()):
                    current_accuracy_test.append(1)
                else:
                    current_accuracy_test.append(0)

            except Exception as e:
                with open("errors.txt", "a") as error_file:
                    error_file.write(str(e) + "\n")
                    torch.cuda.empty_cache()

        test_loss.append(sum(current_loss_test) / len(current_loss_test))
        test_accuracy.append(sum(current_accuracy_test) / len(current_accuracy_test))
        if current_total_hits_test > 0:
            test_hits.append(current_hit_rate_test / current_total_hits_test)
        else:
            test_hits.append(0)

        with open("test_loss.txt", "a") as test_loss_file:
            test_loss_file.write(str(test_loss[-1]) + "\n")
        with open("test_accuracy.txt", "a") as test_accuracy_file:
            test_accuracy_file.write(str(test_accuracy[-1]) + "\n")
        with open("test_hits.txt", "a") as test_hit_file:
            test_hit_file.write(str(test_hits[-1]) + "\n")

    for epoch in range(num_epochs):
        net.train()

        # Initialize accumulation counter
        accum_iter = 0

        for i, data in enumerate(train_loader, 1):
            # It appears some data examples are too large and can't be loaded into VRAM
            # raising a runtime error. These try blocks skip these values and continue
            # training
            gc.collect()
            # torch.cuda.empty_cache()
            # net.train()

            # if i > 0:  # Check untrained performance first
            try:
                protein_1 = data["Protein_1"]
                protein_2 = data["Protein_2"]
                labels = data["Label"].double().to(device)

                # optimizer.zero_grad()

                nodes = protein_1["nodes"].to(device)
                edges = protein_1["edges"].to(device)
                neighbors = protein_1["neighbors"].to(device)
                nodes_2 = protein_2["nodes"].to(device)
                edges_2 = protein_2["edges"].to(device)
                neighbors_2 = protein_2["neighbors"].to(device)
                int_idx = protein_1["int_indices"].to(device)
                int_edges = protein_1["int_edges"].to(device)
                int_neighbors = protein_1["int_neighbors"].to(device)
                int_idx_2 = protein_2["int_indices"].to(device)
                int_edges_2 = protein_2["int_edges"].to(device)
                int_neighbors_2 = protein_2["int_neighbors"].to(device)

                if len(int_idx.shape) == 1:
                    int_idx = torch.unsqueeze(int_idx, 0)
                    int_edges = torch.unsqueeze(int_edges, 0)
                    int_neighbors = torch.unsqueeze(int_neighbors, 0)
                if len(int_idx_2.shape) == 1:
                    int_idx_2 = torch.unsqueeze(int_idx_2, 0)
                    int_edges_2 = torch.unsqueeze(int_edges_2, 0)
                    int_neighbors_2 = torch.unsqueeze(int_neighbors_2, 0)

                outputs = net(
                    nodes,
                    edges,
                    neighbors,
                    nodes_2,
                    edges_2,
                    neighbors_2,
                    int_idx,
                    int_edges,
                    int_neighbors,
                    int_idx_2,
                    int_edges_2,
                    int_neighbors_2,
                )

                outputs = outputs.reshape(-1).double()

                accum_iter += 1

                loss = criterion(outputs, labels)
                loss = loss / 10
                loss.backward()

                # Try clipping gradient
                # nn.utils.clip_grad_norm_(net.parameters(), 1)
                # optimizer.step()

            except Exception as e:
                with open("errors.txt", "a") as error_file:
                    error_file.write(str(e) + "\n")
                    torch.cuda.empty_cache()

            # Every 50 proteins update the weights
            if accum_iter == 10:
                optimizer.step()
                optimizer.zero_grad()
                accum_iter = 0

            if i % 10000 == 0:
                net.eval()
                current_loss_test = []
                current_loss_train = []
                current_accuracy_test = []
                current_accuracy_train = []
                current_hit_rate_test = 0
                total_hits_test = 0
                current_hit_rate_train = 0
                total_hits_train = 0

                with torch.no_grad():
                    for ii, data in enumerate(train_loader_2, 1):
                        gc.collect()
                        # torch.cuda.empty_cache()

                        if ii >= 5000:
                            break

                        try:
                            protein_1 = data["Protein_1"]
                            protein_2 = data["Protein_2"]
                            labels = data["Label"].double().to(device)

                            nodes = protein_1["nodes"].to(device)
                            edges = protein_1["edges"].to(device)
                            neighbors = protein_1["neighbors"].to(device)
                            nodes_2 = protein_2["nodes"].to(device)
                            edges_2 = protein_2["edges"].to(device)
                            neighbors_2 = protein_2["neighbors"].to(device)
                            int_idx = protein_1["int_indices"].to(device)
                            int_edges = protein_1["int_edges"].to(device)
                            int_neighbors = protein_1["int_neighbors"].to(device)
                            int_idx_2 = protein_2["int_indices"].to(device)
                            int_edges_2 = protein_2["int_edges"].to(device)
                            int_neighbors_2 = protein_2["int_neighbors"].to(device)

                            if len(int_idx.shape) == 1:
                                int_idx = torch.unsqueeze(int_idx, 0)
                                int_edges = torch.unsqueeze(int_edges, 0)
                                int_neighbors = torch.unsqueeze(int_neighbors, 0)
                            if len(int_idx_2.shape) == 1:
                                int_idx_2 = torch.unsqueeze(int_idx_2, 0)
                                int_edges_2 = torch.unsqueeze(int_edges_2, 0)
                                int_neighbors_2 = torch.unsqueeze(int_neighbors_2, 0)

                            outputs = net(
                                nodes,
                                edges,
                                neighbors,
                                nodes_2,
                                edges_2,
                                neighbors_2,
                                int_idx,
                                int_edges,
                                int_neighbors,
                                int_idx_2,
                                int_edges_2,
                                int_neighbors_2,
                            )

                            outputs = outputs.reshape(-1).double()

                            loss = criterion(outputs, labels)

                            current_loss_train.append(loss.item())

                            if sig(outputs).item() >= 0.9:
                                current_total_hits_train += 1
                                if int(labels.item()) == 1:
                                    current_hit_rate_train += 1

                            if sig(outputs) >= 0.5:
                                prediction = 1
                            else:
                                prediction = 0

                            if prediction == int(labels.item()):
                                current_accuracy_train.append(1)
                            else:
                                current_accuracy_train.append(0)

                        except Exception as e:
                            with open("errors.txt", "a") as error_file:
                                error_file.write(str(e) + "\n")

                            torch.cuda.empty_cache()

                    train_loss.append(sum(current_loss_train) / len(current_loss_train))
                    train_accuracy.append(
                        sum(current_accuracy_train) / len(current_accuracy_train)
                    )
                    if current_total_hits_train > 0:
                        train_hits.append(
                            current_hit_rate_train / current_total_hits_train
                        )
                    else:
                        train_hits.append(0)

                    with open("train_loss.txt", "a") as train_loss_file:
                        train_loss_file.write(str(train_loss[-1]) + "\n")
                    with open("train_accuracy.txt", "a") as train_accuracy_file:
                        train_accuracy_file.write(str(train_accuracy[-1]) + "\n")
                    with open("train_hits.txt", "a") as train_hit_file:
                        train_hit_file.write(str(train_hits[-1]) + "\n")

                    for iii, data in enumerate(test_loader, 1):
                        gc.collect()
                        # torch.cuda.empty_cache()

                        if iii >= 5000:
                            break

                        try:
                            protein_1 = data["Protein_1"]
                            protein_2 = data["Protein_2"]
                            labels = data["Label"].double().to(device)

                            nodes = protein_1["nodes"].to(device)
                            edges = protein_1["edges"].to(device)
                            neighbors = protein_1["neighbors"].to(device)
                            nodes_2 = protein_2["nodes"].to(device)
                            edges_2 = protein_2["edges"].to(device)
                            neighbors_2 = protein_2["neighbors"].to(device)
                            int_idx = protein_1["int_indices"].to(device)
                            int_edges = protein_1["int_edges"].to(device)
                            int_neighbors = protein_1["int_neighbors"].to(device)
                            int_idx_2 = protein_2["int_indices"].to(device)
                            int_edges_2 = protein_2["int_edges"].to(device)
                            int_neighbors_2 = protein_2["int_neighbors"].to(device)

                            if len(int_idx.shape) == 1:
                                int_idx = torch.unsqueeze(int_idx, 0)
                                int_edges = torch.unsqueeze(int_edges, 0)
                                int_neighbors = torch.unsqueeze(int_neighbors, 0)
                            if len(int_idx_2.shape) == 1:
                                int_idx_2 = torch.unsqueeze(int_idx_2, 0)
                                int_edges_2 = torch.unsqueeze(int_edges_2, 0)
                                int_neighbors_2 = torch.unsqueeze(int_neighbors_2, 0)

                            outputs = net(
                                nodes,
                                edges,
                                neighbors,
                                nodes_2,
                                edges_2,
                                neighbors_2,
                                int_idx,
                                int_edges,
                                int_neighbors,
                                int_idx_2,
                                int_edges_2,
                                int_neighbors_2,
                            )

                            outputs = outputs.reshape(-1).double()

                            loss = criterion(outputs, labels)

                            current_loss_test.append(loss.item())

                            # Hit rate information
                            if sig(outputs).item() >= 0.9:
                                current_total_hits_test += 1
                                if int(labels.item()) == 1:
                                    current_hit_rate_test += 1

                            if sig(outputs) >= 0.5:
                                prediction = 1
                            else:
                                prediction = 0

                            if prediction == int(labels.item()):
                                current_accuracy_test.append(1)
                            else:
                                current_accuracy_test.append(0)

                        except Exception as e:
                            with open("errors.txt", "a") as error_file:
                                error_file.write(str(e) + "\n")

                            torch.cuda.empty_cache()

                    test_loss.append(sum(current_loss_test) / len(current_loss_test))
                    test_accuracy.append(
                        sum(current_accuracy_test) / len(current_accuracy_test)
                    )
                    if current_total_hits_test > 0:
                        test_hits.append(
                            current_hit_rate_test / current_total_hits_test
                        )
                    else:
                        test_hits.append(0)

                    with open("test_loss.txt", "a") as test_loss_file:
                        test_loss_file.write(str(test_loss[-1]) + "\n")
                    with open("test_accuracy.txt", "a") as test_accuracy_file:
                        test_accuracy_file.write(str(test_accuracy[-1]) + "\n")
                    with open("test_hits.txt", "a") as test_hit_file:
                        test_hit_file.write(str(test_hits[-1]) + "\n")

                # Check current minimum every 50000 weight updates
                # This is a rudimentary implementation of early stopping
                # if len(train_loss) >= 20:  # Need at least 20 data points
                #     if conv_i == 20:
                #         conv_i = 0  # Reset the counter to 0 so the iteration works
                #         if (
                #             sum(train_loss[-20:]) / len(train_loss[-20:]) >= current_avg
                #         ):  # Check if the minimum has decreased
                #             PATH = "./net-finished.pth"
                #             torch.save(net.state_dict(), PATH)
                #
                #             return train_loss, test_loss, train_accuracy, test_accuracy
                #
                #         else:
                #             current_avg = sum(train_loss[-20:]) / len(train_loss[-20:])

                # Rudimentary implementation of early stopping that needs to be fixed to
                if len(test_loss) >= 5:  # Need at least 5 data points
                    if conv_i == 5:
                        conv_i = 0  # Reset the counter to 0 so the iteration works
                        new_avg = sum(test_loss[-5:]) / len(test_loss[-5:])
                        if (
                            new_avg
                            < current_best_avg
                            # or math.fabs(new_avg - current_avg) <= epsilon
                        ):  # Check if error is increasing or changed very little
                            stop_counter = 0
                            print("New stop counter value: ", stop_counter, flush=True)
                            PATH = "./net_current_best.pth"
                            torch.save(net.state_dict(), PATH)

                            current_best_avg = sum(test_loss[-5:]) / len(test_loss[-5:])
                            print("New best loss: ", current_best_avg, flush=True)

                        else:
                            stop_counter += 1
                            print("New stop counter value: ", stop_counter, flush=True)

                            if stop_counter == 12:
                                # PATH = "./net_finished.pth"
                                # torch.save(net.state_dict(), PATH)

                                return (
                                    train_loss,
                                    test_loss,
                                    train_accuracy,
                                    test_accuracy,
                                )

                # Reset to train mode
                net.train()
                conv_i += 1  # Iterate the counter
                current_loss_test = []
                current_loss_train = []
                current_accuracy_test = []
                current_accuracy_train = []
                current_hit_rate_test = 0
                current_total_hits_test = 0
                current_hit_rate_train = 0
                current_total_hits_train = 0

        # Checkpoint training every epoch
        EPOCH = epoch + 1
        PATH = "./net-epoch-" + str(EPOCH) + ".pth"
        LOSS = train_loss[-1]

        torch.save(
            {
                "epoch": EPOCH,
                "model_state_dict": net.state_dict(),
                "optimizer_state_dict": optimizer.state_dict(),
                "loss": LOSS,
            },
            PATH,
        )

    # Save final net once all epochs have been completed
    PATH = "./net_finished.pth"
    torch.save(net.state_dict(), PATH)

    return train_loss, test_loss, train_accuracy, test_accuracy


# Train the model
train_loss, test_loss, train_accuracy, test_accuracy = train(25)
