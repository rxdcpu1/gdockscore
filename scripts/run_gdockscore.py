"""Run GDockScore on a single input"""

import argparse
from gdockscore.data.compute_features import get_features, get_interface
import torch
import torch.nn as nn
from gdockscore.models import FullModel


def run_gdockscore(pdb_file, chain_pair):
    # Unpack chains
    chain_1, chain_2 = chain_pair

    # Get node and edges for individual proteins
    nodes_1, edges_1, neighbors_1, prot_info_1 = get_features(pdb_file, chain_1)
    nodes_2, edges_2, neighbors_2, prot_info_2 = get_features(pdb_file, chain_2)

    # Get interface features
    (
        interface_1_indices,
        interface_2_indices,
        neighbors_interface_1,
        neighbors_interface_2,
        int_edges_1to2,
        int_edges_2to1,
    ) = get_interface(pdb_file, prot_info_1, prot_info_2, chain_pair)

    net = FullModel(39, 86, 23, 3, 16, 3, 16, 32, 16, dropout=0.2)
    net = net.double()

    # Send to GPU if available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    net = net.to(device)

    # Load model parameters
    checkpoint = torch.load("./gdockscore/params/weights.pth")
    net.load_state_dict(checkpoint)

    # Set in eval mode
    net.eval()

    # Convert data to tensors
    nodes_1 = torch.squeeze(torch.from_numpy(nodes_1))
    edges_1 = torch.squeeze(torch.from_numpy(edges_1))
    neighbors_1 = torch.squeeze(torch.from_numpy(neighbors_1).long())
    interface_1_indices = torch.squeeze(torch.from_numpy(interface_1_indices).long())
    neighbors_interface_1 = torch.squeeze(
        torch.from_numpy(neighbors_interface_1).long()
    )
    int_edges_1to2 = torch.squeeze(torch.from_numpy(int_edges_1to2))

    nodes_2 = torch.squeeze(torch.from_numpy(nodes_2))
    edges_2 = torch.squeeze(torch.from_numpy(edges_2))
    neighbors_2 = torch.squeeze(torch.from_numpy(neighbors_2).long())
    interface_2_indices = torch.squeeze(torch.from_numpy(interface_2_indices).long())
    neighbors_interface_2 = torch.squeeze(
        torch.from_numpy(neighbors_interface_2).long()
    )
    int_edges_2to1 = torch.squeeze(torch.from_numpy(int_edges_2to1))

    # Send data to device
    nodes_1 = nodes_1.to(device)
    edges_1 = edges_1.to(device)
    neighbors_1 = neighbors_1.to(device)
    interface_1_indices = interface_1_indices.to(device)
    neighbors_interface_1 = neighbors_interface_1.to(device)
    int_edges_1to2 = int_edges_1to2.to(device)

    nodes_2 = nodes_2.to(device)
    edges_2 = edges_2.to(device)
    neighbors_2 = neighbors_2.to(device)
    interface_2_indices = interface_2_indices.to(device)
    neighbors_interface_2 = neighbors_interface_2.to(device)
    int_edges_2to1 = int_edges_2to1.to(device)

    # Add dummy batch dimension
    nodes_1 = torch.unsqueeze(nodes_1, 0)
    edges_1 = torch.unsqueeze(edges_1, 0)
    neighbors_1 = torch.unsqueeze(neighbors_1, 0)
    interface_1_indices = torch.unsqueeze(interface_1_indices, 0)
    int_edges_1to2 = torch.unsqueeze(int_edges_1to2, 0)
    neighbors_interface_1 = torch.unsqueeze(neighbors_interface_1, 0)

    nodes_2 = torch.unsqueeze(nodes_2, 0)
    edges_2 = torch.unsqueeze(edges_2, 0)
    neighbors_2 = torch.unsqueeze(neighbors_2, 0)
    interface_2_indices = torch.unsqueeze(interface_2_indices, 0)
    int_edges_2to1 = torch.unsqueeze(int_edges_2to1, 0)
    neighbors_interface_2 = torch.unsqueeze(neighbors_interface_2, 0)

    # Run the mode
    with torch.no_grad():
        try:
            outputs = net(
                nodes_1,
                edges_1,
                neighbors_1,
                nodes_2,
                edges_2,
                neighbors_2,
                interface_1_indices,
                int_edges_1to2,
                neighbors_interface_1,
                interface_2_indices,
                int_edges_2to1,
                neighbors_interface_2,
            )
        except Exception:
            print("No interaction between inputs found. Please check your inputs...")

    outputs = outputs.reshape(-1).double()

    sig = nn.Sigmoid()

    final_score = sig(outputs)
    final_score = final_score.item()

    with open("results.txt", "a") as f:
        output_line = pdb_file + ": " + str(final_score) + "\n"
        f.write(output_line)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--file",
        dest="pdb_file",
        type=str,
        required=True,
        help="The path to input pdb file.",
    )

    parser.add_argument(
        "--chain_pair",
        dest="chain_pair",
        type=str,
        required=True,
        help="The input chain pair in form 'AB'",
    )

    args = parser.parse_args()
    pdb_file = args.pdb_file
    chain_pair = args.chain_pair

    chain_pair = tuple([x for x in chain_pair])

    run_gdockscore(pdb_file, chain_pair)
